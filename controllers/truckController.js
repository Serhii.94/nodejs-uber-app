const Truck = require('../models/Truck');
const { resGenerator } = require('../helper/responseGenerator');
const { truckTypes } = require('../data');

function checkIfDriver(req) {
	return req.user.role === 'DRIVER';
}

class TruckController {
	async addTruckForUser(req, res) {
		if (checkIfDriver(req)) {
			try {
				const type = req.body.type;
				if (!truckTypes.includes(type)) {
					return resGenerator(res, 400, 'Invalid truck type');
				}
				const truck = new Truck({
					created_by: req.user.id,
					assigned_to: req.user.id,
					type,
				});
				await truck.save();
				return resGenerator(res, 200, 'Truck created successfully');
			} catch (e) {
				console.log(e);
				return resGenerator(res, 500, 'Server error');
			}
		} else {
			return resGenerator(res, 400, 'You are not a driver');
		}
	}

	async getUserTrucks(req, res) {
		if (checkIfDriver(req)) {
			try {
				const infoTrucks = await Truck.find({ created_by: req.user.id });
				const trucks = infoTrucks.map(truck => {
					return {
						_id: truck._id,
						created_by: truck.created_by,
						assigned_to: truck.assigned_to,
						type: truck.type,
						status: truck.status,
						created_date: truck.created_date,
					};
				});
				return res.status(200).json({ trucks });
			} catch (e) {
				console.log(e);
				resGenerator(res, 500, 'Server error');
			}
		} else {
			return resGenerator(res, 400, 'You are not a driver');
		}
	}

	async getUsersTruckById(req, res) {
		if (checkIfDriver(req)) {
			try {
				try {
					const truck = await Truck.findOne({ _id: req.params.id }, '-__v');
					if (truck) {
						return res.status(200).json({ truck });
					}
				} catch (TypeError) {
					return resGenerator(res, 400, 'No truck found');
				}
				return resGenerator(res, 400, 'No truck found');
			} catch (e) {
				console.log(e);
				return resGenerator(res, 500, 'Server error');
			}
		} else {
			return resGenerator(res, 400, 'You are not a driver');
		}
	}

	async updateUsersTruckById(req, res) {
		if (checkIfDriver(req)) {
			try {
				try {
					const truck = await Truck.findOne({ _id: req.params.id }, '-__v');
					if (truck) {
						if (truck.type === req.body.type) {
							return res
								.status(400)
								.json({ message: 'Cannot be changed to the same type' });
						}
						truck.type = req.body.type;
						await truck.save();
						return res
							.status(200)
							.json({ message: 'Truck details changed successfully' });
					}
				} catch (TypeError) {
					return resGenerator(res, 400, 'No truck found');
				}
				return resGenerator(res, 400, 'No truck found');
			} catch (e) {
				console.log(e);
				return resGenerator(res, 500, 'Server error');
			}
		} else {
			return resGenerator(res, 400, 'You are not a driver');
		}
	}

	async deleteUsersTruckById(req, res) {
		if (checkIfDriver(req)) {
			try {
				try {
					const truck = await Truck.findOne({ _id: req.params.id });
					if (truck) {
						await truck.deleteOne({ _id: req.params.id });
						return resGenerator(res, 200, 'Truck deleted successfully');
					}
				} catch (TypeError) {
					return resGenerator(res, 400, 'No truck found');
				}
				return resGenerator(res, 400, 'No truck found');
			} catch (e) {
				console.log(e);
				return resGenerator(res, 500, 'Server error');
			}
		} else {
			return resGenerator(res, 400, 'You are not a driver');
		}
	}

	async asignTruckToUserById(req, res) {
		if (checkIfDriver(req)) {
			try {
				try {
					const truck = await Truck.findOne({ _id: req.params.id });
					if (truck) {
						truck.assigned_to = req.user.id;
						await truck.save();
						return resGenerator(res, 200, 'Truck assigned successfully');
					}
				} catch (TypeError) {
					return resGenerator(res, 400, 'No truck found');
				}
				return resGenerator(res, 400, 'No truck found');
			} catch (e) {
				console.log(e);
				return resGenerator(res, 500, 'Server error');
			}
		} else {
			return resGenerator(res, 400, 'You are not a driver');
		}
	}
}

module.exports = new TruckController();
