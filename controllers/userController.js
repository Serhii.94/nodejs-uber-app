const User = require('../models/User');
const bcrypt = require('bcrypt');
const { resGenerator } = require('../helper/responseGenerator');

class UserController {
	getUserInfo(req, res) {
		try {
			const { id, role, email, iat } = req.user;
			res.status(200).json({
				user: {
					_id: id,
					role,
					email,
					created_date: new Date(iat * 1000),
				},
			});
		} catch (e) {
			console.log(e);
			return resGenerator(res, 500, 'Server error');
		}
	}

	async deleteUser(req, res) {
		try {
			const user = await User.findOne({ _id: req.user.id, role: 'SHIPPER' });
			if (user) {
				await User.deleteOne({ _id: req.user.id });
				return resGenerator(res, 200, 'Profile deleted successfully');
			} else {
				return resGenerator(res, 400, 'You cannot delete your profile');
			}
		} catch (e) {
			console.log(e);
			return resGenerator(res, 500, 'Server error');
		}
	}

	async changeUserPass(req, res) {
		try {
			const user = await User.findOne({ _id: req.user.id });
			const checkPass = await bcrypt.compare(
				req.body.oldPassword,
				user.password
			);
			if (checkPass) {
				user.password = bcrypt.hashSync(req.body.newPassword, 7);
				await user.save();
				return resGenerator(res, 200, 'Password changed successfully');
			} else {
				return resGenerator(res, 400, 'Invalid password');
			}
		} catch (e) {
			console.log(e);
			return resGenerator(res, 500, 'Server error');
		}
	}
}

module.exports = new UserController();
