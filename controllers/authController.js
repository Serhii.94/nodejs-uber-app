const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { secret } = require('../config/config');
const { resGenerator } = require('../helper/responseGenerator');
const { listOfRoles } = require('../data');

const generateAccessToken = (id, email, role) => {
	const payload = {
		id,
		email,
		role,
	};
	return jwt.sign(payload, secret, { expiresIn: '24h' });
};

class AuthController {
	async registration(req, res) {
		try {
			const { email, password, role } = req.body;
			const candidate = await User.findOne({ email });
			if (candidate) {
				return resGenerator(res, 400, 'User already exists');
			} else if (!listOfRoles.includes(role)) {
				return resGenerator(res, 400, `User cannot contain role: ${role}`);
			}
			const hashPassword = bcrypt.hashSync(password, 7);
			const user = new User({ email, password: hashPassword, role });
			await user.save();
			return resGenerator(res, 200, 'Profile created successfully');
		} catch (e) {
			console.log(e);
			return resGenerator(res, 500, 'Server error');
		}
	}

	async login(req, res) {
		try {
			const { email, password } = req.body;
			const user = await User.findOne({ email });
			if (!user) {
				return resGenerator(res, 400, `User with email: ${email} not found`);
			}
			const validPassword = bcrypt.compareSync(password, user.password);
			if (!validPassword) {
				return resGenerator(res, 400, 'Invalid password');
			}
			const token = generateAccessToken(user._id, user.email, user.role);
			return res.status(200).json({ jwt_token: token });
		} catch (e) {
			console.log(e);
			return resGenerator(res, 500, 'Server error');
		}
	}
}

module.exports = new AuthController();
