const Load = require('../models/Load');
const Truck = require('../models/Truck');
const { resGenerator } = require('../helper/responseGenerator');
const { loadStatuses } = require('../data');

function checkIfShipper(req) {
	return req.user.role === 'SHIPPER';
}

class LoadController {
	async addLoadForUser(req, res) {
		if (checkIfShipper(req)) {
			try {
				const load = new Load({
					created_by: req.user.id,
					name: req.body.name,
					payload: req.body.payload,
					pickup_address: req.body.pickup_address,
					delivery_address: req.body.delivery_address,
					dimensions: req.body.dimensions,
				});
				await load.save();
				return resGenerator(res, 200, 'Load created successfully');
			} catch (e) {
				console.log(e);
				return resGenerator(res, 500, 'Server error');
			}
		} else {
			return resGenerator(res, 400, 'You are not a shipper');
		}
	}

	async getUsersLoads(req, res) {
		const { status, offset, limit } = req.query;
		let filter;
		try {
			if (checkIfShipper(req)) {
				filter = { created_by: req.user.id };
			} else {
				filter = { assigned_to: req.user.id };
			}
			let query = Load.find(filter);
			if (loadStatuses.includes(status)) {
				query = query.find({ status });
			}
			if (!isNaN(offset)) {
				query = query.skip(+offset);
			}
			if (!isNaN(limit)) {
				query = query.limit(+limit);
			}
			const loads = await query.select('-__v');
			return res.status(200).json({ loads });
		} catch (e) {
			console.log(e);
			resGenerator(res, 500, 'Server error');
		}
	}

	async getUsersActiveLoad(req, res) {
		if (!checkIfShipper(req)) {
			try {
				const load = await Load.findOne(
					{
						_id: req.params.id,
						status: 'ASSIGNED',
					},
					'-__v'
				);
				return load
					? res.status(200).json({ load })
					: resGenerator(res, 200, 'No load found');
			} catch (e) {
				console.log(e);
				return resGenerator(res, 500, 'Server error');
			}
		} else {
			return resGenerator(res, 400, 'You are not a driver');
		}
	}

	async getUsersLoadById(req, res) {
		try {
			try {
				const load = await Load.findOne({ _id: req.params.id }, '-__v');
				if (load) {
					const loadBody = {
						_id: load._id,
						created_by: load.created_by,
						assigned_to: load.assigned_to,
						status: load.status,
						state: load.state,
						name: load.name,
						payload: load.payload,
						pickup_address: load.pickup_address,
						delivery_address: load.delivery_address,
						dimensions: load.dimensions,
						logs: load.logs,
						created_date: load.created_date,
					};
					return res.status(200).json({ load: loadBody });
				}
			} catch (TypeError) {
				return resGenerator(res, 400, 'No load found');
			}
			return resGenerator(res, 400, 'No load found');
		} catch (e) {
			console.log(e);
			return resGenerator(res, 500, 'Server error');
		}
	}

	async updateUsersLoadById(req, res) {
		if (checkIfShipper(req)) {
			try {
				try {
					const load = await Load.findOne({ _id: req.params.id });
					if (load && load.status === 'NEW') {
						await Load.findByIdAndUpdate(req.params.id, {
							name: req.body.name,
							payload: req.body.payload,
							pickup_address: req.body.pickup_address,
							delivery_address: req.body.delivery_address,
							dimensions: req.body.dimensions,
						});
						return resGenerator(res, 200, 'Load details changed successfully');
					}
				} catch (TypeError) {
					return resGenerator(res, 400, 'No load found');
				}
				return resGenerator(res, 400, 'No load found');
			} catch (e) {
				console.log(e);
				return resGenerator(res, 500, 'Server error');
			}
		} else {
			return resGenerator(res, 400, 'You are not a shipper');
		}
	}

	async deleteUsersLoadById(req, res) {
		if (checkIfShipper(req)) {
			try {
				try {
					await Load.findOneAndDelete({ _id: req.params.id });
					return resGenerator(res, 200, 'Load deleted successfully');
				} catch (e) {
					console.log(e);
					return resGenerator(res, 400, 'No load found');
				}
			} catch (e) {
				console.log(e);
				return resGenerator(res, 500, 'Server error');
			}
		} else {
			return resGenerator(res, 400, 'You are not a shipper');
		}
	}

	async postUsersLoadById(req, res) {
		if (checkIfShipper(req)) {
			try {
				const load = await Load.findOne({ _id: req.params.id });
				if (load.status === 'NEW') {
					await Load.findOneAndUpdate({ status: 'POSTED' });
					load.logs.push({
						message: 'Load assigned to driver with id ###',
						time: new Date(),
					});
					load.save();
					return res.status(200).json({
						message: 'Load posted successfully',
						driver_found: true,
					});
				}
				return resGenerator(res, 400, 'No load found');
			} catch (e) {
				console.log(e);
				return resGenerator(res, 500, 'Server error');
			}
		} else {
			return resGenerator(res, 400, 'You are not a shipper');
		}
	}

	async getUsersLoadShippingInfo(req, res) {
		if (checkIfShipper(req)) {
			try {
				const load = await Load.findOne({
					_id: req.params.id,
				}).select('-__v');
				if (load) {
					await Truck.findOne({ assigned_to: load.assigned_to })
						.select('-__v')
						.then(truck => {
							return res.status(200).json({ load, truck });
						});
				} else {
					return resGenerator(res, 400, 'No load found');
				}
			} catch (e) {
				console.error(e);
				return resGenerator(res, 500, 'Server error');
			}
		} else {
			return resGenerator(res, 400, 'You are not a shipper');
		}
	}
}

module.exports = new LoadController();
