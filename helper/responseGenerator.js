module.exports.resGenerator = (res, statusCode, message) =>
	res.status(statusCode).json({ message });
