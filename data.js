const listOfRoles = ['SHIPPER', 'DRIVER'];
const truckTypes = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];
const truckStatuses = ['OL', 'IS'];
const loadStatuses = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];
const loadStates = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];

module.exports = {
  listOfRoles,
  truckTypes,
  truckStatuses,
  loadStatuses,
  loadStates,
};
