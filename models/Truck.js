const { Schema, model } = require('mongoose');

const { truckTypes, truckStatuses } = require('../data');

const Truck = new Schema({
	id: {
		type: String,
	},
	created_by: {
		type: String,
		required: true,
	},
	assigned_to: {
		type: String,
		default: null,
	},
	type: {
		type: String,
		enum: truckTypes,
		required: true,
	},
	status: {
		type: String,
		default: 'IS',
		enum: truckStatuses,
		required: true,
	},
	created_date: {
		type: Date,
		default: Date.now(),
	},
});
module.exports = model('Truck', Truck);
