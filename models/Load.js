const { Schema, model } = require('mongoose');
const { loadStates, loadStatutes } = require('../data');

const Load = new Schema({
	created_by: {
		type: String,
		required: true,
	},
	assigned_to: {
		type: String,
		default: null,
	},
	status: {
		type: String,
		enum: loadStatutes,
		default: 'NEW',
		required: true,
	},
	state: {
		type: String,
		enum: loadStates,
		default: 'En route to Pick Up',
	},
	name: {
		type: String,
		required: true,
	},
	payload: Number,
	dimensions: {
		width: Number,
		length: Number,
		height: Number,
	},
	pickup_address: {
		type: String,
		required: true,
	},
	delivery_address: {
		type: String,
		required: true,
	},
	logs: {
		type: [
			{
				message: { type: String, required: true },
				time: { type: Date, required: true },
			},
		],
		default: [],
		_id: false,
	},
	created_date: {
		type: Date,
		default: Date.now(),
	},
});
module.exports = model('Load', Load);
