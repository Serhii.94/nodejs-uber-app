const { Schema, model } = require('mongoose');
const { listOfRoles } = require('../controllers/authController');

const User = new Schema({
	email: {
		type: String,
		unique: true,
		required: true,
	},
	password: {
		type: String,
		required: true,
	},
	role: {
		type: String,
		required: true,
		enum: listOfRoles,
	},
});
module.exports = model('User', User);
