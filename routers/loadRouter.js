const Router = require('express');
const router = new Router();
const controller = require('../controllers/loadController');
const authMiddleware = require('../middleware/authMiddleware');

router.get('/loads', authMiddleware, controller.getUsersLoads);
router.post('/loads', authMiddleware, controller.addLoadForUser);
router.get('/loads/active', authMiddleware, controller.getUsersActiveLoad);
router.get('/loads/:id', controller.getUsersLoadById);
router.put('/loads/:id', authMiddleware, controller.updateUsersLoadById);
router.delete('/loads/:id', authMiddleware, controller.deleteUsersLoadById);
router.post('/loads/:id/post', authMiddleware, controller.postUsersLoadById);
router.get(
	'/loads/:id/shipping_info',
	authMiddleware,
	controller.getUsersLoadShippingInfo
);

module.exports = router;
