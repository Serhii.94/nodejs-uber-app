const Router = require('express');
const router = new Router();
const controller = require('../controllers/AuthController');

router.post('/auth/register', controller.registration);
router.post('/auth/login', controller.login);

module.exports = router;
