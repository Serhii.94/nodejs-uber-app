const Router = require('express');
const router = new Router();
const controller = require('../controllers/userController');
const authMiddleware = require('../middleware/authMiddleware');

router.get('/users/me', authMiddleware, controller.getUserInfo);
router.delete('/users/me', authMiddleware, controller.deleteUser);
router.patch('/users/me/password', authMiddleware, controller.changeUserPass);

module.exports = router;
