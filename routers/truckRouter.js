const Router = require('express');
const router = new Router();
const controller = require('../controllers/truckController');
const authMiddleware = require('../middleware/authMiddleware');

router.get('/trucks', authMiddleware, controller.getUserTrucks);
router.post('/trucks', authMiddleware, controller.addTruckForUser);
router.get('/trucks/:id', authMiddleware, controller.getUsersTruckById);
router.put('/trucks/:id', authMiddleware, controller.updateUsersTruckById);
router.delete('/trucks/:id', authMiddleware, controller.deleteUsersTruckById);
router.post(
	'/trucks/:id/assign',
	authMiddleware,
	controller.asignTruckToUserById
);

module.exports = router;
