require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

const app = express();
const port = +process.env.PORT;
const DB_CONNECTION_STRING = process.env.DB_CONNECTION_STRING;

app.use(express.json());
app.use(
    morgan(':method :url :status :res[content-length] - :response-time ms'),
);

app.use('/api', authRouter);
app.use('/api', userRouter);
app.use('/api', truckRouter);
app.use('/api', loadRouter);

const start = async () => {
  try {
    await mongoose.connect(DB_CONNECTION_STRING);
    app.listen(port, () =>
      console.log(`Server is running on http://localhost:${port}`),
    );
  } catch (err) {
    console.error(`Failed to start server: ${err.message}`);
    return;
  }
};
start();
